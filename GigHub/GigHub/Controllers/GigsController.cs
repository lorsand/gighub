﻿using GigHub.Models;
using GigHub.ViewModels;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System;

namespace GigHub.Controllers
{
    public class GigsController : Controller
    {
        ApplicationDbContext context;

        public GigsController()
        {
            this.context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult Create()
        {
            var genres = context.Genres.ToList();

            var gigViewModel = new GigViewModel() {
                Genres = genres
            };

            return View(gigViewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GigViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!ModelState.IsValid)
            {
                model.Genres = context.Genres.ToList();

                return View(model);
            }

            this.SaveNewGig(model);

            return RedirectToAction("Index", "Home");
        }

        private void SaveNewGig(GigViewModel model)
        {
            var currentUserId = this.GetCurrentUserId();

            var newGig = new Gig()
            {
                ArtistId = currentUserId,
                GenreId = model.Genre,
                Venue = model.Venue,
                DateTime = model.GetDateTime()
            };

            context.Gigs.Add(newGig);
            context.SaveChanges();
        }

        private string GetCurrentUserId()
        {
            return User.Identity.GetUserId();
        }
    }
}