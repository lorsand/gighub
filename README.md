# GigHub

Gighub is a mini social network that makes it really easy
for live music lovers to track the gigs of their favorite
artists.

##Reqirements

Artists can sign up and list their gigs. When adding a gig,
they should specify the date/time, location and genre of the gig.

An artist should have a page called my Upcoming Gigs.
From there, they should be abe to edit or remove an existing gig, or add another gig to the list.

Users should be able to view upcoming gigs or search them by artist, genre or location.
They should be able to view details of a gig and add it to their calendar.

Additionally, users should be able to follow their favorite artists.
When they follow an artist, they should see the upcoming gigs of their favorite artist in the Gig Feed.